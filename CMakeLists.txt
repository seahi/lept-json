cmake_minimum_required(VERSION 3.10)
project(lept_json)

set(CMAKE_STANDARD 99)

add_executable(main test.c leptjson.c)